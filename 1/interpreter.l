%{
     #define STRLEN 8192
     #include<stdlib.h>
     #include<unistd.h>
     #include"interpreter.tab.h"
     #include"string.h"
     void lexError();
     char* stringHandler(char *string);

     
%}

intNum    \-?([0-9])+
realNum   \-?{intNum}\.{intNum}
add       \+
sub       \-
mul       \*
div       \/
mod       \%
pow       \^
left      \(
right     \)
comma     \,
strpow    pow|poW|pOw|pOW|Pow|PoW|POw|POW
string    ([0-9a-zA-Z\!\@\#\$\%\^\&\*\(\)\_\+\-\=\[\{\]\}\:\,\<\.\>\/\? ])+

%%

{intNum}       {yylval.intnum=atoi(yytext); return(INTNUM);}
{realNum}      {yylval.realnum=atof(yytext); return(REALNUM);}
{add}          {return(ADD);}
{sub}          {return(SUB);}
{mul}          {return(MUL);}
{div}          {return(DIV);}
{mod}          {return(MOD);}
{pow}          {return(POW);}
{strpow}       {return(SPOW);}
{left}         {return(LEFT);}
{right}        {return(RIGHT);}
{comma}        {return(COMMA);}
clear\(\)      {system("clear");}
exit\(\)       {_exit(0);}
\"{string}\"   {yylval.string=stringHandler(yytext);return(STRING);}
;              {return(SEMICOLON);}
.              {lexError(); return(OTHER);}
%%
void lexError()
{
     perror("Input no valido\n");
}

char *stringHandler(char *string)
{
     char *str = (char *)malloc(STRLEN*sizeof(char));
     sscanf(string,"\"%[^\n]s",str);
     str[len(str)-1]='\0';
     return(str);
}
