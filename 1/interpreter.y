%{
     #include<stdio.h>
     #include<math.h>
     int yylex();
     int yyerror(char *s);
     char *stradd(char*,char*);
     char *strmul(char*,int);
%}

%token INTNUM REALNUM SEMICOLON ADD SUB MUL DIV MOD POW SPOW LEFT RIGHT COMMA STRING OTHER 

%left ADD SUB
%left MUL DIV MOD 
%left POW SPOW

%type <string> STRING
%type <intnum> INTNUM
%type <realnum>REALNUM
%type <string> STREXP
%type <intnum> INTEXP
%type <realnum>REALEXP
%union
{
     char *string;
     int intnum;
     double realnum;
}
%start instructions
%%

instructions:
     | 
     instruction SEMICOLON instructions
;

instruction:
     EXP|
     TYP|
     OTHER
;

TYP:
   INTNUM|
   REALNUM|
   STRING
;

EXP:
     INTEXP
     {
          printf("%d",$1);
     }|
     REALEXP
     {
          printf("%g",$1);
     }|
     STREXP
     {
          printf("%s",$1);
     }
;

INTEXP:
     INTNUM|
     INTEXP ADD INTEXP
     {
          $$=$1+$3;
     }|
     INTEXP SUB INTEXP
     {
          $$=$1-$3;
     }|
     INTEXP MUL INTEXP
     {
          $$=$1*$3;
     }|
     INTEXP DIV INTEXP
     {
          $$=$1/$3;
     }|
     INTEXP MOD INTEXP
     {
          $$=$1%$3;
     }|
     SPOW LEFT INTEXP COMMA INTEXP RIGHT
     {
          $$=pow((double)$3,(double)$5);
     }|
     INTEXP POW INTEXP
     {
          $$=pow((double)$1,(double)$3);
     }

;

REALEXP:
     REALNUM|
     REALEXP ADD REALEXP
     {
          $$=$1+$3;
     }|
     REALEXP SUB REALEXP
     {
          $$=$1-$3;
     }|
     REALEXP MUL REALEXP
     {
          $$=$1*$3;
     }|
     REALEXP DIV REALEXP
     {
          $$=$1/$3;
     }|
     REALEXP POW REALEXP
     {
          $$=pow($1,$3);
     }|
     INTEXP ADD REALEXP
     {
          $$=$1+$3;
     }|
     INTEXP SUB REALEXP
     {
          $$=$1-$3;
     }|
     INTEXP MUL REALEXP
     {
          $$=$1*$3;
     }|
     INTEXP DIV REALEXP
     {
          $$=$1/$3;
     }|
     SPOW LEFT REALEXP COMMA INTEXP RIGHT
     {
          $$=pow((double)$3,(double)$5);
     }|
     INTEXP POW REALEXP
     {
          $$=pow($1,$3);
     }|
     REALEXP ADD INTEXP
     {
          $$=$1+$3;
     }|
     REALEXP SUB INTEXP
     {
          $$=$1-$3;
     }|
     REALEXP MUL INTEXP
     {
          $$=$1*$3;
     }|
     REALEXP DIV INTEXP
     {
          $$=$1/$3;
     }|
     REALEXP POW INTEXP
     {
          $$=pow($1,$3);
     }
;

STREXP:
     STRING|
     STREXP ADD STREXP
     {
          char *s=stradd($1,$3);
          $$=s;
     }|
     POW LEFT STREXP COMMA INTEXP RIGHT
     {
          char *s=strmul($3,$5);
          $$=s;
     }|
     STREXP POW INTNUM
     {
          char *s=strmul($1,$3);
          $$=s;
     }
;



%%
int yyerror(char *s)
{
     printf("syntax Error %s\n", s);
     return(-1);
}

int main()
{
     yyparse();
     return(0);
}
