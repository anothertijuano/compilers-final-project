#include<math.h>
data_type cast(data *x, data *y);

data ADD(data_type dataType, data x, data y);
data SUB(data_type dataType, data x, data y);
data MUL(data_type dataType, data x, data y);
data MOD(data_type dataType, data x, data y);
data DIV(data_type dataType, data x, data y);
data POW(data_type dataType, data x, data y);
//LOGIC
data AND(data x, data y);
data OR(data x, data y);
data NOT(data x);

data_type cast(data *x, data *y)
{
     data_type data1=x->type;
     data_type data2=y->type;
     data_type type={NONE};
     if(data1==NONE || data2==NONE)
          type=NONE;
     else if(data1==data2)
          type=data1;
     else if((data1==INT||data1==REAL)&&(data2==INT||data2==REAL))
          {
               type=REAL;
               int tmp=0;
               if(data1==INT)
               {
                    x->type=REAL;
                    tmp=x->Integer;
                    x->Real=tmp;
               } else
               {
                    y->type=REAL;
                    tmp=y->Integer;
                    y->Real=tmp;
               }
          }
     return(type);
}

data COPY(data src)
{
     data r;
     data_type dataType=src.type;
     if(dataType==INT)
     {
          r.type=INT;
          r.Integer=src.Integer;
     } else if(dataType==REAL)
     {
          r.type=REAL;
          r.Real=src.Real;
     } else if(dataType==STRING)
     {
          r.type=STRING;
          copy(&src.String,&r.String);
     }
     return(r);
}

data ADD(data_type dataType, data x, data y)
{
     data r={NONE};
     if(dataType==INT)
     {
          r.type=INT;
          r.Integer=x.Integer+y.Integer;
     } else if(dataType==REAL)
     {
          r.type=REAL;
          r.Real=x.Real+y.Real;
     } else if(dataType==STRING)
     {
          r.type=STRING;
          r.String=stradd(x.String,y.String);
     }
     return(r);
}

data SUB(data_type dataType, data x, data y)
{
     data r={NONE};
     if(dataType==INT)
     {
          r.type=INT;
          r.Integer=x.Integer-y.Integer;
     } else if(dataType==REAL)
     {
          r.type=REAL;
          r.Real=x.Real-y.Real;
     } /*else if(dataType==STRING)
     {
          r.type=STRING;
          r.String=stradd(x.String,y.String);
     }*/
     return(r);
}

data MOD(data_type dataType, data x, data y)
{
     data r={NONE};
     if(dataType==INT)
     {
          r.type=INT;
          r.Integer=x.Integer%y.Integer;
     } /*else if(dataType==REAL)
     {
          r.type=REAL;
          r.Real=x.Real*y.Real;
     } else if(dataType==STRING)
     {
          r.type=STRING;
          r.String=strmul(x.String,y.Integer);
     }*/
     return(r);
}

data MUL(data_type dataType, data x, data y)
{
     data r={NONE};
     if(dataType==INT)
     {
          r.type=INT;
          r.Integer=x.Integer*y.Integer;
     } else if(dataType==REAL)
     {
          r.type=REAL;
          r.Real=x.Real*y.Real;
     } else if(x.type==STRING && y.type==INT)
     {
          r.type=STRING;
          r.String=strmul(x.String,y.Integer);
     }
     return(r);
}

data DIV(data_type dataType, data x, data y)
{
     data r={NONE};
     if(dataType==INT)
     {
          r.type=INT;
          r.Integer=x.Integer/y.Integer;
     } else if(dataType==REAL)
     {
          r.type=REAL;
          r.Real=x.Real/y.Real;
     } /*else if(dataType==STRING)
     {
          r.type=STRING;
          r.String=stradd(x.String,y.String);
     }*/
     return(r);
}

data POW(data_type dataType, data x, data y)
{
     data r={NONE};
     if(dataType==INT)
     {
          r.type=REAL;
          r.Real=pow(x.Integer,y.Integer);
     } else if(dataType==REAL)
     {
          r.type=REAL;
          r.Real=pow(x.Real,y.Real);
     } else if(dataType==STRING)
     {
          r.type=STRING;
          r.String=strmul(x.String,y.Integer);
     }
     return(r);
}

//Logical
data AND(data x, data y)
{
     data r={NONE};
     if(x.type!=BOOL || y.type==BOOL)
          return(r);
     r.type=BOOL;
     r.Bool=x.Bool&y.Bool;
}
data OR(data x, data y)
{
     data r={NONE};
     if(x.type!=BOOL || y.type==BOOL)
          return(r);
     r.type=BOOL;
     r.Bool=x.Bool|y.Bool;
}
data NOT(data x)
{
     data r={NONE};
     if(x.type!=BOOL)
          return(r);
     r.type=BOOL;
     unsigned char tmp=~x.Bool;
     if(tmp=0xFE)
          r.Bool=0;
     else
          r.Bool=1;
}

