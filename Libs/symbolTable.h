#include"table.h"
#include"operations.h"
#include<error.h>

int writetotable(data_type type, void *val, char *lex)
{
     data tmp;
     tmp.type=type;
     if(type==INT)
          tmp.Integer=*(int *)val;
     else if(type==REAL)
          tmp.Real=*(double *)val;
     else if(type==STRING)
          tmp.String=*(char **)val;
     else
          return(-1);
     int ret=install(tmp,lex);
     return(ret);
}

data readfromtable(char *lex)
{
     struct nlist *tmp=lookup(lex);
     if(!tmp)
     {
          //My NULL (NONE)
          data ret;
          ret.type=NONE;
          return(ret);
     }
     return(tmp->variable);
}

void duplicate(char *lex)
{
     install(readfromtable(lex),"_duplicate");
}

void op(char *opcode, char *var1, char *var2)
{
     data x=readfromtable(var1);
     data y=readfromtable(var2);
     data_type type;
     if(!strcpr(opcode,"CPY"))
     {
          install(COPY(x),var2);
     }else if(!strcpr(opcode,"ADD"))
     {
          //type=cast(&x,&y);
          if(type==NONE)
               error(-1,-1,"Incompatible datatype for ADD");
          else
               install(ADD(type,x,y),"_tmp");
     }else if(!strcpr(opcode,"SUB"))
     {
          type=cast(&x,&y);
          if(type==NONE)
               error(-1,-1,"Incompatible datatype for SUB");
          else
               install(SUB(type,x,y),"_tmp");
     }else if(!strcpr(opcode,"MUL"))
     {
          type=cast(&x,&y);
          if(x.type==STRING && y.type==INT)
               install(MUL(type,x,y),"_tmp");
          else if(type==NONE)
                    error(-1,-1,"Incompatible datatype for MUL");
          else
                    install(MUL(type,x,y),"_tmp");
     }else if(!strcpr(opcode,"MOD"))
     {
          type=cast(&x,&y);
          if(type==NONE)
               error(-1,-1,"Incompatible datatype for MOD");
          else
               install(MOD(type,x,y),"_tmp");
     }else if(!strcpr(opcode,"DIV"))
     {
          type=cast(&x,&y);
          if(type==NONE)
               error(-1,-1,"Incompatible datatype for DIV");
          else
               install(DIV(type,x,y),"_tmp");
     }else if(!strcpr(opcode,"POW"))
     {
          if(x.type==STRING && y.type==INT)
               install(POW(STRING,x,y),"_tmp");
          else
          {
               type=cast(&x,&y);
               if(x.type==STRING||y.type==STRING)
                    error(-1,-1,"Incompatible datatype for POW");
               else
                    install(POW(type,x,y),"_tmp");
          }
     }else if(!strcpr(opcode,"AND"))
     {
          data tmp=AND(x,y);
          if(tmp.type==NONE)
               error(-1,-1,"Incompatible datatype for logical AND");
          else
               install(tmp,"_tmp");
     }else if(!strcpr(opcode,"OR"))
     {
          data tmp=OR(x,y);
          if(tmp.type==NONE)
               error(-1,-1,"Incompatible datatype for logical OR");
          else
               install(tmp,"_tmp");
     }else if(!strcpr(opcode,"NOT"))
     {
          data tmp=NOT(x);
          if(tmp.type==NONE)
               error(-1,-1,"Incompatible datatype for logical NOT");
          else
               install(tmp,"_tmp");
     }
}
int print(char *lex)
{
     struct nlist *tmp=lookup(lex);
     if(!tmp)
          return(-2);
     if(tmp->variable.type == INT)
          printf("%d",tmp->variable.Integer);
     else if(tmp->variable.type == REAL)
          printf("%f",tmp->variable.Real);
     else if(tmp->variable.type == STRING)
          printf("%s",tmp->variable.String);
     else
          return(-1);
     return(0);
}
