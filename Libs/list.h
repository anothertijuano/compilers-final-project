#include<stddef.h>
#include<stdlib.h>
#include<stdio.h> //debug

struct listNode
{
     struct listNode *next;
     struct listNode *prev;
     void *data;
};

struct listNode *node_init(void *data)
{
     struct listNode *tmp;
     tmp=(struct listNode *)malloc(sizeof(struct listNode));
     tmp->next=NULL;
     tmp->prev=NULL;
     tmp->data=data;
     return tmp;
}

//-----------
struct list
{
     struct listNode *start;
     struct listNode *end;
     int numElements;
};

struct list *list_init()
{
     struct list *tmp;
     tmp=(struct list *)malloc(sizeof(struct list));
     tmp->start=NULL;
     tmp->end=NULL;
     tmp->numElements=0;
     return(tmp);
}

int list_append(struct list *l, void *data)
{
     struct listNode *n=node_init(data);
     if(l->numElements > 0)
     {
          n->prev=l->end;
          l->end->next=n;
          l->end=n;
          l->numElements+=1;
     }
     else
     {
          l->start=n;
          l->end=n;

     }
     return(0);
}
int list_remove(struct list *l, struct listNode *n)
{
     struct listNode ptr;
     if(l->start==ptr)
          l->start=ptr->next;
     if(l->end==ptr)
          l->end=ptr->prev;
     for(ptr=list->start;ptr!=NULL;ptr=ptr->next)
          if(ptr==n)
          {
               if(ptr->prev)
                    ptr->prev->next=ptr->next;
               if(ptr->next)
                    ptr->next->prev=ptr->prev;
          }
     return(0);
}
