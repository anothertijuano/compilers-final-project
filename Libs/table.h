#include<stddef.h>
#include<stdlib.h>
#include"string.h"
#include"datatype.h"
#define HASHSIZE 100 

struct nlist
{
     struct nlist *next;
     data variable;
     char *lex;
};


int install(data var,char *s);
struct nlist *lookup(char *s);
unsigned hash(char *);

static struct nlist *hashtab[HASHSIZE];

int init()
{
     int i;
     for(i;i<HASHSIZE;i++)
          hashtab[i]=malloc(sizeof(struct nlist));
     return(0);
}


unsigned hash(char *s)
{
     unsigned hashval;
     for (hashval=0; *s!='\0'; s++)
          hashval=*s+31*hashval;
     return (hashval % HASHSIZE);
}


struct nlist *lookup(char *s)
{
     struct nlist *np;
     for(np = hashtab[hash(s)]; np!=NULL; np=np->next)
          if(!strcpr(s, np->lex))
               return np;
     return NULL;
}


int install(data var,char *s)
{
     struct nlist *np;
     unsigned int hashval;
     np=lookup(s);
     if(!np)
     {
          //Elemento no existe en tabla
          np=(struct nlist *) malloc(sizeof(struct nlist));
          np->variable=var;
          np->lex=s;
          np->next=NULL;
          unsigned index=hash(s);
          struct nlist *tmp=(struct nlist *) malloc(sizeof(*np));
          tmp=hashtab[index];
          if(tmp)
          {
               for(tmp;(tmp->next)!=NULL; tmp=tmp->next);
                    tmp->next=np;
          }
          else
               hashtab[index]=np;
          return(0);
     } else
     {
          //Elemento existe en tabla 
          np->variable=var;
          np->lex=s;
          return(0); //solía ser 1      
     }
}

