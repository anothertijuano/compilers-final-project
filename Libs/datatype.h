typedef enum DATA_TYPE {INT, REAL, STRING, BOOL, NONE} data_type;

typedef struct DATA 
{
     data_type type;
     union
     {
          int Integer;
          double Real;
          char * String;
          unsigned char Bool;
     };
}data;
