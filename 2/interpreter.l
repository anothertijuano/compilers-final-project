%{
     /*
     #include<stdlib.h>
     #include<unistd.h>
     #include"interpreter.tab.h"
     //#include"../Libs/string.h"*/
%}

intNum    ([0-9])+
realNum   {intNum}\.{intNum}
bit       TRUE|FALSE
add       \+
sub       \-
mul       \*
div       \/
mod       \%
pow       \^
left      \(
right     \)
equal     \=
comma     \,
strpow    [Pp][Oo][Ww]
string    ([^\"])*
var       [a-z][a-zA-Z0-9\-\_]*
int       int
real      real
str       string
bool      bit
space     [ ]
%%

{intNum}       {yylval.intnum=atoi(yytext); return(INTNUM);}
{realNum}      {yylval.realnum=atof(yytext); return(REALNUM);}
{bit}          {yylval.bit=stob(yytext); return(BIT);}
{equal}        {return(EQ);}
{add}          {return(ADD_);}
{sub}          {return(SUB_);}
{mul}          {return(MUL_);}
{div}          {return(DIV_);}
{mod}          {return(MOD_);}
{pow}          {return(POW_);}
{strpow}       {return(SPOW_);}
{left}         {return(LEFT);}
{right}        {return(RIGHT);}
{comma}        {return(COMMA);}
{int}          {return(INT_BYSON);}
{real}         {return(REAL_BYSON);}
{str}          {return(STR_BYSON);}
{bool}         {return(BOOL_BYSON);}
{space}        {return(SPACE);}
if             {return(IF);}
\>             {return(MORE);}
\<             {return(LESS);}
{var}          {char*tmp; copy2(yytext,&tmp); yylval.string=tmp;return(VAR);}
--\"{string}\" {}

print\({var}\)\; {printvar(yytext);}
msg:\"{string}\" {msgHandler(yytext);}     
clear\(\)\;    {system("clear");}
exit\(\)\;     {_exit(0);}
\"{string}\"   {yylval.string=stringHandler(yytext);return(STRING_BYSON);}
;              {return(SEMICOLON);}
.              {lexError(); return(OTHER);}
%%
