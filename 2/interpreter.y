%token INTNUM REALNUM BIT SEMICOLON EQ ADD_ SUB_ MUL_ DIV_ MOD_ POW_ SPOW_ LEFT RIGHT COMMA STRING_BYSON OTHER INT_BYSON REAL_BYSON STR_BYSON BOOL_BYSON VAR SPACE COMMENT IF LESS MORE

%left ADD_ SUB_
%left MUL_ DIV_ MOD_ 
%left POW_ SPOW_
%left LEFT RIGHT
%type <string> STRING_BYSON
%type <intnum> INTNUM
%type <realnum>REALNUM
%type <bit>    BIT
%type <string> STREXP
%type <intnum> INTEXP
%type <realnum>REALEXP
%type <string> VAR
%type <string> VAREXP
%type <string> EXP
%type <string> DEF
%union
{
     char *string;
     int intnum;
     double realnum;
     unsigned char bit;
}
%start instructions
%%

instructions:
     | 
     instruction SEMICOLON instructions
;

instruction:
     DEC|
     EXP|
     DEF|
     STM
;

STM:
     IF LEFT VAR MORE VAR RIGHT
     {
          data var1=readfromtable($3);
          data var2=readfromtable($5);
          if(var1.type!=STRING || var2.type!=STRING)
               error(-1,-1,"Non valid dataype for operation");
          int x1=len(var1.String);
          int x2=len(var2.String);
          if(x1>x2)
               printf("1\n");
          else
               printf("0\n");

     }|
     IF LEFT VAR LESS VAR RIGHT
     {
          data var1=readfromtable($3);
          data var2=readfromtable($5);
          if(var1.type!=STRING || var2.type!=STRING)
               error(-1,-1,"Non valid dataype for operation");
          int x1=len(var1.String);
          int x2=len(var2.String);
          if(x1<x2)
               printf("1\n");
          else
               printf("0\n");
     }
;
DEC:
     INT_BYSON SPACE VAR EQ EXP
     {
          data tmp=readfromtable($5);
          if(tmp.type==REAL)
          {
               int x=tmp.Real;
               writetotable(INT,&x,$3);
          }
          else if(tmp.type==NONE || tmp.type==INT)
               writetotable(INT,&tmp.Integer,$3);
          else
               error(-1,-1,"Non valid dataype for variable");
     }|
     REAL_BYSON SPACE VAR EQ EXP
     {
          data tmp=readfromtable($5);
          if(tmp.type==INT)
          {
               double x=tmp.Integer;
               writetotable(REAL,&x,$3);
          }
          else if(tmp.type==NONE || tmp.type==REAL)
               writetotable(REAL,&tmp.Real,$3);
          else
               error(-1,-1,"Non valid dataype for variable");
     }|
     STR_BYSON SPACE VAR EQ EXP
     {
          data tmp=readfromtable($5);
          if(tmp.type==NONE || tmp.type==STRING)
               writetotable(STRING,&tmp.String,$3);
          else
          {
               printf("%d",tmp.type);
               error(-1,-1,"Non valid dataype for variable");
          }
     }
;

DEF:
     VAR EQ INTEXP
     {
          data tmp=readfromtable($1);
          if(tmp.type==REAL)
          {
               double x=$3;
               writetotable(REAL,&x,$1);
          }
          else if(tmp.type==INT)
               writetotable(INT,&$3,$1);
          else
               error(-1,-1,"Non valid dataype for variable");
     }|
     VAR EQ REALEXP
     {
          data tmp=readfromtable($1);
          if(tmp.type==INT)
          {
               int x=$3;
               writetotable(INT,&x,$1);
          }
          else if(tmp.type==REAL)
               writetotable(REAL,&$3,$1);
          else
               error(-1,-1,"Non valid dataype for variable");
     }|
     VAR EQ STREXP
     {
          data tmp=readfromtable($1);
          if(tmp.type==STRING)
               writetotable(STRING,&$3,$1);
          else
               error(-1,-1,"Non valid dataype for variable");
     }|
     VAR EQ VAREXP
     {
          data tmp=readfromtable($1);
          data tmp2=readfromtable($3);
          if(tmp.type==NONE || tmp2.type==NONE)
               error(-1,-1,"Non valid dataype for variable");
          else if(tmp.type==tmp2.type)
               if(tmp.type==STRING)
                    writetotable(STRING,&tmp2.String,$1);
               else if(tmp.type==INT)
                    writetotable(INT,&tmp2.Integer,$1);
               else if(tmp.type==REAL)
                    writetotable(REAL,&tmp2.Real,$1);
          else
               error(-1,-1,"Non valid dataype for variable");
     }
;

EXP:
     INTEXP
     {
          writetotable(INT,&$1,"_tmp");
          char *r="_tmp";
          //print(r);
          $$=r;
     }|
     REALEXP
     {
          writetotable(REAL,&$1,"_tmp");
          char *r="_tmp";
          //print(r);
          $$=r;
     }|
     STREXP
     {
          writetotable(STRING,&$1,"_tmp");
          char *r="_tmp";
          //print(r);
          $$=r;
     }|
     VAREXP
     {
          //print($1);
          $$=$1;
     }
;

INTEXP:
     INTNUM|
     SUB_ INTEXP
     {
          $$=-$2;
     }|
     LEFT INTEXP RIGHT
     {
          $$=$2;
     }| 
     INTEXP ADD_ INTEXP
     {
          $$=$1+$3;
     }|
     INTEXP SUB_ INTEXP
     {
          $$=$1-$3;
     }|
     INTEXP MUL_ INTEXP
     {
          $$=$1*$3;
     }|
     INTEXP DIV_ INTEXP
     {
          $$=$1/$3;
     }|
     INTEXP MOD_ INTEXP
     {
          $$=$1%$3;
     }|
     SPOW_ LEFT INTEXP COMMA INTEXP RIGHT
     {
          $$=pow((double)$3,(double)$5);
     }|
     INTEXP POW_ INTEXP
     {
          $$=pow((double)$1,(double)$3);
     }
;

REALEXP:
     REALNUM|
     SUB_ REALEXP
     {
          $$=-$2;
     }|
     LEFT REALEXP RIGHT
     {
          $$=$2;
     }| 
     REALEXP ADD_ REALEXP
     {
          $$=$1+$3;
     }|
     REALEXP SUB_ REALEXP
     {
          $$=$1-$3;
     }|
     REALEXP MUL_ REALEXP
     {
          $$=$1*$3;
     }|
     REALEXP DIV_ REALEXP
     {
          $$=$1/$3;
     }|
     REALEXP POW_ REALEXP
     {
          $$=pow($1,$3);
     }|
     INTEXP ADD_ REALEXP
     {
          $$=$1+$3;
     }|
     INTEXP SUB_ REALEXP
     {
          $$=$1-$3;
     }|
     INTEXP MUL_ REALEXP
     {
          $$=$1*$3;
     }|
     INTEXP DIV_ REALEXP
     {
          $$=$1/$3;
     }|
     SPOW_ LEFT REALEXP COMMA INTEXP RIGHT
     {
          $$=pow((double)$3,(double)$5);
     }|
     INTEXP POW_ REALEXP
     {
          $$=pow($1,$3);
     }|
     REALEXP ADD_ INTEXP
     {
          $$=$1+$3;
     }|
     REALEXP SUB_ INTEXP
     {
          $$=$1-$3;
     }|
     REALEXP MUL_ INTEXP
     {
          $$=$1*$3;
     }|
     REALEXP DIV_ INTEXP
     {
          $$=$1/$3;
     }|
     REALEXP POW_ INTEXP
     {
          $$=pow($1,$3);
     }
;

STREXP:
     STRING_BYSON|
     LEFT STREXP RIGHT
     {
          $$=$2;
     }| 
     STREXP ADD_ STREXP
     {
          char *s=stradd($1,$3);
          $$=s;
     }|
     POW_ LEFT STREXP COMMA INTEXP RIGHT
     {
          char *s=strmul($3,$5);
          $$=s;
     }|
     STREXP POW_ INTEXP
     {
          char *s=strmul($1,$3);
          $$=s;
     }
;

VAREXP:
     VAR
     {
          $$=$1;
     }|
     LEFT VAREXP RIGHT
     {
          $$=$2;
     }| 
     VAREXP ADD_ VAREXP
     {
          printf("holi\n");
          op("ADD",$1,$3);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP SUB_ VAREXP
     {
          op("SUB",$1,$3);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP MUL_ VAREXP
     {
          op("MUL",$1,$3);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP DIV_ VAREXP
     {
          op("DIV",$1,$3);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP MOD_ VAREXP
     {
          op("MOD",$1,$3);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP POW_ VAREXP
     {
          op("POW",$1,$3);
          char *r="_tmp";
          $$=r;
     }|
     INTEXP ADD_ VAREXP
     {
          writetotable(INT,&$1,"_int");
          op("ADD","_int",$3);
          char *r="_tmp";
          $$=r;
     }|
     INTEXP SUB_ VAREXP
     {
          writetotable(INT,&$1,"_int");
          op("SUB","_int",$3);
          char *r="_tmp";
          $$=r;
     }|
     INTEXP MUL_ VAREXP
     {
          writetotable(INT,&$1,"_int");
          op("MUL","_int",$3);
          char *r="_tmp";
          $$=r;
     }|
     INTEXP DIV_ VAREXP
     {
          writetotable(INT,&$1,"_int");
          op("DIV","_int",$3);
          char *r="_tmp";
          $$=r;
     }|
     INTEXP MOD_ VAREXP
     {
          writetotable(INT,&$1,"_int");
          op("MOD","_int",$3);
          char *r="_tmp";
          $$=r;
     }|
     INTEXP POW_ VAREXP
     {
          writetotable(INT,&$1,"_int");
          op("POW","_int",$3);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP ADD_ INTEXP
     {
          writetotable(INT,&$3,"_int");
          op("ADD","_int",$1);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP SUB_ INTEXP
     {
          writetotable(INT,&$3,"_int");
          op("SUB","_int",$1);
          char *r="_tmp";
          $$=r;
          printf("I hate my life\n");
     }|
     VAREXP MUL_ INTEXP
     {
          writetotable(INT,&$3,"_int");
          op("MUL","_int",$1);
          char *r="_tmp";
          $$=r;
          printf("I hate my life\n");
     }|
     VAREXP DIV_ INTEXP
     {
          writetotable(INT,&$3,"_int");
          op("DIV","_int",$1);
          char *r="_tmp";
          $$=r;
          printf("I hate my life\n");
     }|
     VAREXP MOD_ INTEXP
     {
          writetotable(INT,&$3,"_int");
          op("MOD","_int",$1);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP POW_ INTEXP
     {
          writetotable(INT,&$3,"_int");
          op("POW",$1,"_int");
          char *r="_tmp";
          $$=r;
     }|
     REALEXP ADD_ VAREXP
     {
          writetotable(REAL,&$1,"_real");
          op("ADD","_real",$3);
          char *r="_tmp";
          $$=r;
     }|
     REALEXP SUB_ VAREXP
     {
          writetotable(REAL,&$1,"_real");
          op("SUB","_real",$3);
          char *r="_tmp";
          $$=r;
     }|
     REALEXP MUL_ VAREXP
     {
          writetotable(REAL,&$1,"_real");
          op("MUL","_real",$3);
          char *r="_tmp";
          $$=r;
     }|
     REALEXP DIV_ VAREXP
     {
          writetotable(REAL,&$1,"_real");
          op("DIV","_real",$3);
          char *r="_tmp";
          $$=r;
     }|
     REALEXP POW_ VAREXP
     {
          writetotable(REAL,&$1,"_real");
          op("POW","_real",$3);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP ADD_ REALEXP
     {
          writetotable(REAL,&$3,"_real");
          op("ADD","_real",$1);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP SUB_ REALEXP
     {
          writetotable(REAL,&$3,"_real");
          op("SUB","_real",$1);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP MUL_ REALEXP
     {
          writetotable(REAL,&$3,"_real");
          op("MUL","_real",$1);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP DIV_ REALEXP
     {
          writetotable(REAL,&$3,"_real");
          op("DIV","_real",$1);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP POW_ REALEXP
     {
          writetotable(REAL,&$3,"_real");
          op("POW","_real",$1);
          char *r="_tmp";
          $$=r;
     }|
     STREXP ADD_ VAREXP
     {
          writetotable(STRING,&$1,"_string");
          op("ADD","_string",$3);
          char *r="_tmp";
          $$=r;
     }|
     STREXP POW_ VAREXP
     {
          writetotable(STRING,&$1,"_string");
          op("POW","_string",$3);
          char *r="_tmp";
          $$=r;
     }|
     VAREXP ADD_ STREXP
     {
          writetotable(REAL,&$3,"_string");
          op("ADD","_string",$1);
          char *r="_tmp";
          $$=r;
     }
;

%%
